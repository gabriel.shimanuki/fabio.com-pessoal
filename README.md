## Super agregador de blogs e notícias
**Sumário**

[[_TOC_]]

### Introdução
---

Esse projeto é uma aplicação Web, com o intuito de criar um agregador de notícias e blogs customizável por um usuário.
Usando a liguagem Java e aplicando o padrão de projeto MVC, constuímos a primeira fase do projeto utilizando as seguintes dependências:

* Spring Boot DevTools
* Spring Web
* Thymeleaf
* Spring Data JPA
* MySQL
* Spring Data JDBC

Note que todas as dependências são baixadas pelo Maven.

### Como usar
----------

1. Instale o MySQL em sua máquina;
2. Crie um banco de dados no MySQL:

	1. Primeiramente, crie uma **conexão** usando a _script_ `cria-usuario.sql`;
	2. Rode essa primeira _script_;
	3. Depois crie uma segunda **conexão**, utilizando como _Connection Name_ e como _Username_`fabicom`;
	4. Clique no botão _Test Connection_. Será pedida uma senha. Então, digite: `mac0321`;
	5. Insira a script `cria-db.sql`;
	6. Rode a _script_;

3. Feito isso, o banco de dados já está configurado. Então, abra o projeto no eclipse;

4. No projeto, abra a pasta `src/main/java` -> (_package_) `com.fabio.agregador` -> (arquivo) `AgregadorDeBlogsENoticiasApplication.java`;

5. Execute com o **_Java Application_**;

6. Abra uma aba de navegador e digite: `http://localhost:9091`;

Espera-se que o programa rode normalmente.

### Notas
----

Caso ocorra algum problema com o MySQL, utilizamos [esse tutorial](https://www.youtube.com/watch?v=PZVaYMtW1wc) para configurarmos o nosso banco de dados.